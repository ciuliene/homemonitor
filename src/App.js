import React, { Component } from "react";
import "./App.css";
import LightSwitch from "./components/LightSwitch";

class App extends Component {
  state = {};
  render() {
    return (
      <div className="App">
        <LightSwitch
          title={"Light room 1"}
          text={"Push the button to lit or unlit the light"}
        />
        <LightSwitch
          title={"Light room 2"}
          text={"Push the button to lit or unlit the light"}
        />
        <LightSwitch
          title={"Light room 2"}
          text={"Push the button to lit or unlit the light"}
        />
      </div>
    );
  }
}

export default App;
