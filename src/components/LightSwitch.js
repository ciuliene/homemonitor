import React, { Component } from "react";
import { Card, Button } from "react-bootstrap";
import "./style/card.css";

export default class LightSwitch extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: props.title,
      text: props.text,
      status: 0,
    };
  }

  render() {
    return (
      <Card className="card" style={{}}>
        <Card.Body>
          <Card.Title>{this.state.title}</Card.Title>
          <Card.Text className="cardText">{this.state.text}</Card.Text>
          <Button variant="primary">Go somewhere</Button>
        </Card.Body>
      </Card>
    );
  }
}
